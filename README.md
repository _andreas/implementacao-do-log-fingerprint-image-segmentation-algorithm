# README #

Esta é uma tentativa de aplicação do algoritmo descrito no paper
"A Simple and Novel FIngerprint Image Segmentation Algorithm",
disponível em: 
https://www.researchgate.net/publication/271546369_A_simple_and_novel_fingerprint_image_segmentation_algorithm

### Referências utilizadas ###
Os algoritmos utilizados para resolver o problema (Laplacian of Gaussain, LoG Threshold, operações morfológicas, etc)
foram baseados em diversos papers e livros, tal como Gonzales, Rafael C - "Digital Image Processing" e 
o material do site http://homepages.inf.ed.ac.uk/rbf/HIPR2/morops.htm para operações morfológicas.

A detecção de borda foi feita de acordo com o algoritmo listado no paper "Theory of Edge Detection", por D. Marr e E. Hildreth.

Demais referências estão listadas no código em comentários.

### O que este programa faz ###

Este programa gerará imagens .bmp de cada passo relevante do algoritmo (LoG, detecção de borda pelo threshold definido, computação do gradiente em y e as operações morfológicas de fill (não funcional), opening, closing e dilation) para as nove imagens presentes na pasta FVC2004_DB3, no repositório.

