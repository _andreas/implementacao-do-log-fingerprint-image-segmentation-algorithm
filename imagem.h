
#ifndef __IMAGEM_H
#define __IMAGEM_H

using namespace std;

/* Imagem RGB (1 canaL). */
typedef struct
{
	unsigned long largura;
	unsigned long altura;
	unsigned char** dados; /* Matriz de dados. */
} Imagem1C;

/* Imagem RGB (3 canais). */

typedef struct
{
	unsigned long largura;
	unsigned long altura;
	unsigned char*** dados; /* 3 matrizes de dados. */
} Imagem3C;

#include <vector>

#define KERNEL_SIZE 7
#define PI 3.14159265358979323846

Imagem1C* criaImagem1C(int largura, int altura);
void destroiImagem1C(Imagem1C* img);
Imagem1C* abreImagem1C(char* arquivo);
int salvaImagem1C(Imagem1C* img, char* arquivo);
void normalize(std::vector<double> & img, int img_x, int img_y);
void export_normalize(std::vector<double> & img, unsigned char *out, int img_x, int img_y);
void thresholdLoG(std::vector<double> & img, std::vector<double> & out, int img_x, int img_y, double threshold);
void binGradientY(std::vector<double> & img, std::vector<double> & out, int img_x, int img_y);
void morphologicalErosion(std::vector<double> & img, std::vector<double> &  out, int img_x, int img_y, unsigned char element[5][5], int element_size);
void morphologicalDilate(std::vector<double> & img, std::vector<double> &  out, int img_x, int img_y, unsigned char element[5][5], int element_size);
void saveBMP2(std::vector<double> in, unsigned char *img, Imagem1C* out, char* arquivo, int img_x, int img_y);
void saveBMP(unsigned char *img, Imagem1C* out, char* arquivo, int img_x, int img_y);
void morphologicalIntersection(std::vector<double> &img, std::vector<double> &img2, std::vector<double> &out, int img_x, int img_y, unsigned char element[5][5], int element_size);
int algLoG(char* pasta, char* arquivo, int IMAGE_X, int IMAGE_Y);
void morphologicalFill(std::vector<double> &img,  std::vector<double> &out, int img_x, int img_y);
void normalize(std::vector<double> & img, int img_x, int img_y);
void export_normalize(std::vector<double> & img, unsigned char *out, int img_x, int img_y);
void calculaLoGKernel(double sigma, int size);
void convolucao(std::vector<double> & img, std::vector<double> & out, int kernel_size, int img_x, int img_y);


/*============================================================================*/
/* DOIS TIPOS PARA MANIPULA��O DE ARQUIVOS BMP                                */
/*----------------------------------------------------------------------------*/
/* Autor: Bogdan T. Nassu - nassu@dainf.ct.utfpr.edu.br                       */
/*============================================================================*/
/** Este arquivo traz declara��es para dois tipos e rotinas para manipula��o de
* arquivos bmp. Como temos um prop�sito puramente did�tico, apenas um sub-
* conjunto m�nimo do formato foi implementado. Matrizes s�o usadas para
* representar os dados. Vetores seriam computacionalmente mais eficientes, mas
* aqui procuramos priorizar a clareza e a facilidade de uso. */
/*============================================================================*/
/*============================================================================*/
/* Imagem em escala de cinza (1 canal). */

/*----------------------------------------------------------------------------*/
/* Por simplicidade e compatibilidade, as fun��es para ler e escrever imagens
* de 1 canal na verdade convertem a imagem para 3 canais. Isto quer dizer que
* s� podemos ler imagens com 24bpp. */

/*============================================================================*/

Imagem3C* criaImagem3C(int largura, int altura);
void destroiImagem3C(Imagem3C* img);
Imagem3C* abreImagem3C(char* arquivo);
int salvaImagem3C(Imagem3C* img, char* arquivo);
/*============================================================================*/

// Fun��es e constantes de leitura de arquivos tif dispobinilizadas no site que hospeda
// as databases utilizadas no artigo. Arquivo dispon�vel em:
// http://bias.csr.unibo.it/fvc2004/Downloads/sourceFVC.zip

#define FVC2000_H
#define MAXPATH 255
#define MAXIMAGESIZE 640*640
#define SUCCESS 0
#define SYNTAX_ERROR 1
#define CANNOT_OPEN_CONFIG_FILE 2
#define CANNOT_OPEN_OUTPUT_FILE 3
#define CANNOT_OPEN_IMAGE_FILE 4
#define CANNOT_OPEN_TEMPLATE_FILE 5
#define TIF_LOAD_ERROR 6
#define CANNOT_WRITE_TEMPLATE 7
#define CANNOT_UPDATE_OUTPUT_FILE 8
#define XXXX_INIT_ERROR 100
#define XXXX_SETUP_ERROR 101

int Load_gray256tif(FILE* fp, bool Upright, int img_x, int img_y);

/*============================================================================*/
#endif /* __IMAGEM_H */
