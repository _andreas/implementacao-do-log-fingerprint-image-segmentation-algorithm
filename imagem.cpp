#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <vector>
#include "imagem.h"
#include <algorithm>


unsigned char IMAGE[MAXIMAGESIZE]; // utilizada na biblioteca
double kernel[KERNEL_SIZE][KERNEL_SIZE];
unsigned char element[5][5];

using namespace std;
int main(){
	int IMAGE_X, IMAGE_Y, i, j;
	IMAGE_X = 300;
	IMAGE_Y = 480;
	//=========== criando o sctructuring element que ser� utilizado
	for (i = 0; i < 5; i++) {
		for (j = 0; j < 5; j++) {
			element[i][j] = 0;
		}
	}
	for (i = 0; i < 5; i++) {
		element[2][j] = 1;
	}

	for (i = 0; i < 3; i++) {
		element[1][j] = 1;
		element[3][j] = 1;
	}

	element[0][2] = 1;
	element[4][2] = 1;

	algLoG("FVC2004_DB3/","101_2", IMAGE_X, IMAGE_Y);
	algLoG("FVC2004_DB3/", "105_1", IMAGE_X, IMAGE_Y);
	algLoG("FVC2004_DB3/", "105_2", IMAGE_X, IMAGE_Y);
	algLoG("FVC2004_DB3/", "105_3", IMAGE_X, IMAGE_Y);
	algLoG("FVC2004_DB3/", "105_4", IMAGE_X, IMAGE_Y);
	algLoG("FVC2004_DB3/", "105_5", IMAGE_X, IMAGE_Y);
	algLoG("FVC2004_DB3/", "105_6", IMAGE_X, IMAGE_Y);
	algLoG("FVC2004_DB3/", "105_7", IMAGE_X, IMAGE_Y);
	algLoG("FVC2004_DB3/", "105_8", IMAGE_X, IMAGE_Y);
	return (0);
}

int algLoG(char* pasta, char* arquivo, int IMAGE_X, int IMAGE_Y){
	int i, j;
	Imagem1C* img;
	FILE *im;
	int err;
	char imagefile[MAXPATH];
	bool Upright;

	std::vector<double> IMAGE_FLOAT;
	std::vector<double> IMAGE_CONVOLUTED;
	std::vector<double> IMAGE_AUX;


	//=========== 1) abrindo imagem ============
	char foostring[64];
	sprintf_s(foostring, 64, "%s%s.tif", pasta, arquivo);
	strcpy_s(imagefile, foostring);
	img = criaImagem1C(300, 480);
	err = fopen_s(&im, imagefile, "rb");

	if (err == 0) {
		cout << "Arquivo " << pasta << arquivo << ".tif aberto."<< endl;
		Upright = true;
		// Upright=true requires the image to be loaded Upright: IMAGE[0] denotes the "top-left" pixel 
		// Upright=false requires the image to be loaded Upsidedown: IMAGE[0] denotes the "bottom-left" pixel 
	}
	else {
		return CANNOT_OPEN_IMAGE_FILE;
	}
	if (im == NULL) return CANNOT_OPEN_IMAGE_FILE;
	err = Load_gray256tif(im, Upright, IMAGE_X, IMAGE_Y);
	fclose(im);
	if (err) return TIF_LOAD_ERROR;
	
	IMAGE_FLOAT.resize(IMAGE_X * IMAGE_Y);
	IMAGE_CONVOLUTED.resize(IMAGE_X * IMAGE_Y);
	IMAGE_AUX.resize(IMAGE_X * IMAGE_Y);

	// usar img->dados[j][i]!
	for (i = 0; i < IMAGE_X; i++) {
		for (j = 0; j < IMAGE_Y; j++) {
			img->dados[j][i] = IMAGE[j*IMAGE_X + i];
			IMAGE_CONVOLUTED[j*IMAGE_X + i] = ((double)IMAGE[j*IMAGE_X + i]);
			IMAGE_FLOAT[j*IMAGE_X + i] = ((double)IMAGE[j*IMAGE_X + i]);

		}
	}

	for (i = 0; i < IMAGE_X; i++) {
		for (j = 0; j < IMAGE_Y; j++) {
			img->dados[j][i] = (unsigned char)IMAGE_CONVOLUTED[j*IMAGE_X + i];
		}
	}
	//=========== Normaliza a imagem ============
	cout << "Normalizando imagem." << endl;
	normalize(IMAGE_FLOAT, IMAGE_X, IMAGE_Y);

	//=========== Aplicando a LoG na imagem ============

	//=========== encontra o Kernel que ser� utilizado na fun��o
	calculaLoGKernel(3.5, KERNEL_SIZE);
	/*
	for (i = 0; i < KERNEL_SIZE; i++) {
		for (j = 0; j < KERNEL_SIZE; j++) {
			printf("%.5f |", kernel[i][j]);
		}
		puts("");
	}
	*/
	//=========== fazendo a convolu��o
	cout << "Aplicando LoG." << endl;
	convolucao(IMAGE_FLOAT, IMAGE_CONVOLUTED, KERNEL_SIZE, IMAGE_X, IMAGE_Y);
	char foostring2[64];
	sprintf_s(foostring2, 64, "%s LoG.bmp", arquivo);
	saveBMP2(IMAGE_CONVOLUTED, IMAGE, img, foostring2, IMAGE_X, IMAGE_Y);

	//=========== Aplica threshold utilizand LoG edge detection
	cout << "LoG edge detection." << endl;
	double threshold = 0.01;
	calculaLoGKernel(2, KERNEL_SIZE);
	thresholdLoG(IMAGE_CONVOLUTED, IMAGE_FLOAT, IMAGE_X, IMAGE_Y, threshold);
	char foostring3[64];
	sprintf_s(foostring3, 64, "%s threshold.bmp", arquivo);
	saveBMP2(IMAGE_FLOAT, IMAGE, img, foostring3, IMAGE_X, IMAGE_Y);

	//=========== Computa gradiente na dire��o y
	cout << "Computando gradiente Y." << endl;
	binGradientY(IMAGE_FLOAT, IMAGE_CONVOLUTED, IMAGE_X, IMAGE_Y);
	char foostring4[64];
	sprintf_s(foostring4, 64, "%s gradientY.bmp", arquivo);
	saveBMP2(IMAGE_FLOAT, IMAGE, img, foostring4, IMAGE_X, IMAGE_Y);

	//=========== Opera��es morfol�gicas
	// Fill
	// a opera��o de fill n�o funciona devidamente, e como minhas instru��es
	// eram n�o utilizar bibliotecas externas, s� consegui chegar at� aqui. Imagino que
	// o resultado final seria bastante pr�ximo do desejado caso esta opera��o funcionasse.
	// O resultado acaba tendo um contorno branco indesejado ao redor da imagem, seguindo a ordem
	// das opera��es mencionadas no paper. A falta de uma fun��o fill que funcione acabou impactando
	// meu resultado final.
	// O paper tamb�m � vago quanto a quais opera��es exatas, usando um "ie" antes de list�-las.
	cout << "Aplicando morphological fill." << endl;
	morphologicalFill(IMAGE_CONVOLUTED, IMAGE_FLOAT, IMAGE_X, IMAGE_Y);
	char foostring7[64];
	sprintf_s(foostring7, 64, "%s fill.bmp", arquivo);
	saveBMP2(IMAGE_CONVOLUTED, IMAGE, img, foostring7, IMAGE_X, IMAGE_Y);
	/*
	// Opening - erosion -> dilate com mesmo structuring element
	cout << "Aplicando morphological opening." << endl;
	morphologicalErosion(IMAGE_CONVOLUTED, IMAGE_FLOAT, IMAGE_X, IMAGE_Y, element, 5);
	morphologicalDilate(IMAGE_FLOAT, IMAGE_CONVOLUTED, IMAGE_X, IMAGE_Y, element, 5);
	char foostring5[64];
	sprintf_s(foostring5, 64, "%s opening.bmp", arquivo);
	saveBMP2(IMAGE_CONVOLUTED, IMAGE, img, foostring5, IMAGE_X, IMAGE_Y);
	*/
	// Closing - dilate -> erosion com o mesmo structuring element
	cout << "Aplicando morphological closing." << endl;
	morphologicalDilate(IMAGE_CONVOLUTED, IMAGE_FLOAT, IMAGE_X, IMAGE_Y, element, 5);
	morphologicalErosion(IMAGE_FLOAT, IMAGE_CONVOLUTED, IMAGE_X, IMAGE_Y, element, 5);

	char foostring6[64];
	sprintf_s(foostring6, 64, "%s closing.bmp", arquivo);
	saveBMP2(IMAGE_CONVOLUTED, IMAGE, img, foostring6, IMAGE_X, IMAGE_Y);

	morphologicalDilate(IMAGE_CONVOLUTED, IMAGE_FLOAT, IMAGE_X, IMAGE_Y, element, 5);
	char foostring8[64];
	sprintf_s(foostring8, 64, "%s dilate.bmp", arquivo);
	saveBMP2(IMAGE_FLOAT, IMAGE, img, foostring8, IMAGE_X, IMAGE_Y);
	destroiImagem1C(img);
	cout << "Sucesso! \n";
}




/*============================================================================*/
void normalize(std::vector<double> &img, int img_x, int img_y) {
	int i, j;
	double  min, max;
	max = -9999.0;
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			if(img[j*img_x + i] > max)
				max = img[j*img_x + i];
		}
	}
	min = max;
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			if (img[j*img_x + i] < min)
				min = img[j*img_x + i];
		}
	}
	
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			img[j*img_x + i] = (img[j*img_x + i] - min) / (max - min);
		}
	}
	//printf("min: %f, max: %f\n", min, max);
}

void export_normalize(std::vector<double> &img, unsigned char *out, int img_x, int img_y) {
	int i, j;
	double  min, max;
	max = -9999.0;
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			if (img[j*img_x + i] > max)
				max = img[j*img_x + i];
		}
	}
	min = max;
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			if (img[j*img_x + i] < min)
				min = img[j*img_x + i];
		}
	}

	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			img[j*img_x + i] = (img[j*img_x + i] - min) / (max - min);
		}
	}
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			out[j*img_x + i] = (unsigned char)((img[j*img_x + i] - min) / (max - min) * 255);
		}
	}
}

void calculaLoGKernel(double sigma, int size) {
	int i, j;
	double p1, p2, p3;
	double sum = 0;
	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			p1 = -(1 / (PI * pow(sigma, 4)));
			p2 = 1 - ((pow(i + 1, 2) + pow(j + 1, 2)) / (2 * pow(sigma, 2)));
			p3 = exp(-((pow(i + 1, 2) + pow(j + 1, 2)) / (2 * pow(sigma, 2))));
			kernel[i][j] = p1*p2*p3*255;
			sum += p1*p2*p3;
		}
	}
	//printf("Kernel sum: %f\n", sum);
}


void convolucao(std::vector<double> &img, std::vector<double> &out, int kernel_size, int img_x, int img_y) {
	int i, j, k, l;
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			out[j*img_x + i] = 1;
		}
	}
	for (i = 3; i < img_x - 3; i++) {
		for (j = 3; j < img_y - 3; j++) {
			for (k = -3; k <= 3; k++) {
				for (l = -3; l <= 3; l++) {
					out[i + j*img_x] += (img[i + k + (j + l)*img_x] * kernel[k + 3][l + 3]);
				}
			}
		}
	}
}

// fonte do algoritmo: http://www.hms.harvard.edu/bss/neuro/bornlab/qmbc/beta/day4/marr-hildreth-edge-prsl1980.pdf
// http://homepages.inf.ed.ac.uk/rbf/HIPR2/zeros.htm
// threshold est� sendo usado como refer�ncia para o "zero crossing" ao inv�s do
// zero. Como no paper, o valor de threshold � determinado empiricamente
void thresholdLoG(std::vector<double> &img, std::vector<double> &out, int img_x, int img_y, double threshold) {
	int i, j, k, l;
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			out[j*img_x + i] = 0;
		}
	}
	int neg, pos;
	double sum;
	for (i = 4; i < img_x - 4; i++) {
		for (j = 4; j < img_y - 4; j++) {
			// checa os quatro vizinhos cardinais para verificar se teve
			// uma mudan�a de valor positivo para negativo em algum ponto
			// ao menos.
			neg = pos = 0;
			if (img[(j - 1)*img_x + i] <= threshold) { // vizinho superior
				neg++;
			}
			else if(img[(j - 1)*img_x + i] > threshold){
				pos++;
			}
			if(img[j*img_x + i - 1] <= threshold ){ // vizinho esquerdo
				neg++;
			}
			else if(img[j*img_x + i - 1] > threshold) {
				pos++;
			}
			if (img[j*img_x + i + 1] <= threshold) { // vizinho direito
				neg++;
			}
			else if (img[j*img_x + i + 1] > threshold) {
				pos++;
			}
			if (img[(j + 1)*img_x  + i] <= threshold) { // vizinho inferior
				neg++;
			}
			else if (img[(j + 1)*img_x + i] > threshold) {
				pos++;
			}
			if ((neg > 0) & (pos > 0)) {
				out[j*img_x + i] = 1;
			}
			else {
				out[j*img_x + i] = 0;
			}
		}
	}
}

void binGradientY(std::vector<double> &img, std::vector<double> &out, int img_x, int img_y) {
	int i, j;
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			out[j*img_x + i] = 0;
		}
	}
	for (i = 4; i < img_x - 4; i++) {
		for (j = 4; j < img_y - 4; j++) {
			out[j*img_x + i] = ((img[(j + 1)*img_x + i] - img[(j - 1)*img_x + i])/2);
			
			if(out[j*img_x + i] <= 0)
				out[j*img_x + i] = 0;
			else
				out[j*img_x + i] = 1;
			
		}
	}
}
/*
As opera��es morfol�gicas nada mais s�o que convolu��es entre uma matriz pr� definida,
chamada de structuring element, e a imagem original, seguindo uma l�gica que determina 
quais pixels ficam e quais saem, dependendo de qual opera��o.
// structuring element:

C�rculo de raio 5:
01110
11111
11111
11111
01110

Tamb�m poss�vel usar o diamante:
00100
01110
11111
01110
00100
*/

// Esta opera��o de fill n�o � uma leg�tima, a verdadeira de acordo com a literatura (Gonzales, Rafael C. Digital Image Processing)
// utiliza de um algoritm de hole filling e as opera��es dilate, complementation and intersection. O problema � que
// n�o consegui encontrar nenhuma explica��o sobre um algoritmo para ENCONTRAR os buracos. A alternativa para
// a solu��o do problema, no livro � Morphological Reconstruction, mas n�o consegui implementar uma vers�o que funcione.
// Imagino que esta opera��o de fill � o principal culpado pelos resultados que atingi, analisando o output da fase
// do gradiente, antes das opera��es morfol�gicas, o contorno da impress�o digital est� bem definido, faltando apenas
// um meio de preencher os buracos.
void morphologicalFill(std::vector<double> &img, std::vector<double> &out, int img_x, int img_y) {
	int i, j;
	for (i = 5; i < img_x - 5; i++) {
		for (j = 5; j < img_y - 5; j++) {
			if ((img[(j)*img_x + i] == 0) & (img[(j - 1)*img_x + i] == 1) & (img[j*img_x + i - 1] == 1)) { // vizinho superior
				out[j*img_x + i] = 1;
			}
		}
	}
	for (i = 5; i < img_x - 5; i++) {
		for (j = 5; j < img_y - 5; j++) {
			if((out[j*img_x + i] == 1) & (img[(j)*img_x + i] == 0)){ // vizinho superior
				img[j*img_x + i] = 1;
			}
		}
	}
}

// Feita de acordo com http://homepages.inf.ed.ac.uk/rbf/HIPR2/dilate.htm
// Dilate � uma das duas opera��es b�sicas, junto de erosion
void morphologicalDilate(std::vector<double> &img, std::vector<double>  &out, int img_x, int img_y, unsigned char element[5][5], int element_size) {
	int i, j, k, l;
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			out[j*img_x + i] = 0;
		}
	}
	bool flag;
	for (i = 5; i < img_x - 5; i++) {
		for (j = 5; j < img_y - 5; j++) {
			flag = true;
			if (img[i + j*img_x] == 0) {
				for (k = -element_size; (k < 5) & flag; k++) {
					for (l = -element_size; (l < 5) & flag; l++) {
						if (element[k + element_size][l + element_size] == 0) {
							if (img[i + k + (j + l)*img_x] == 1) {
								flag = false;
							}
						}
					}
				}
			}
			if (!flag) {
				out[i + j*img_x] = 1;
			}
			else {
				out[i + j*img_x] = 0;
			}
		}
	}
}

// Feita de acordo com http://homepages.inf.ed.ac.uk/rbf/HIPR2/erode.htm
// Erosion � uma das duas opera��es b�sicas, junto de erosion
void morphologicalErosion(std::vector<double> &img, std::vector<double> &out, int img_x, int img_y, unsigned char element[5][5], int element_size) {
	int i, j, k, l;
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			out[j*img_x + i] = 0;
		}
	}
	bool flag;
	for (i = 5; i < img_x - 5; i++) {
		for (j = 5; j < img_y - 5; j++) {
			flag = true;
			if (img[i + j*img_x] == 1) {
				for (k = -element_size; (k < 5) & flag; k++) {
					for (l = -element_size; (l < 5) & flag; l++) {
						if (element[k + element_size][l + element_size] == 1) {
							if (img[i + k + (j + l)*img_x] == 0) {
								flag = false;
							}
						}
					}
				}
			}
			if (!flag) {
				out[i + j*img_x] = 1;
			}
			else {
				out[i + j*img_x] = 0;
			}
		}
	}
}

void saveBMP(unsigned char *img, Imagem1C* out, char* arquivo, int img_x, int img_y){
	int i, j;
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			out->dados[j][i] = img[j*img_x + i];
		}
	}
	salvaImagem1C(out, arquivo);
}

void saveBMP2(std::vector<double> in, unsigned char *img, Imagem1C* out, char* arquivo, int img_x, int img_y){
	int i, j;
	cout << arquivo << " salvo." << endl;
	normalize(in, img_x, img_y);
	export_normalize(in, img, img_x, img_y);
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			out->dados[j][i] = img[j*img_x + i];
		}
	}
	salvaImagem1C(out, arquivo);
}

void morphologicalIntersection(std::vector<double> &img, std::vector<double> &img2, ::vector<double> &out, int img_x, int img_y, unsigned char element[5][5], int element_size) {
	int i, j, k, l;
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			out[j*img_x + i] = 0;
		}
	}
	for (i = 4; i < img_x - 4; i++) {
		for (j = 4; j < img_y - 4; j++) {
			if ((img[i + j*img_x] == 1) & (img2[i + j*img_x] == 1)) {
				out[j*img_x + i] = 1;
			}
			else {
				out[j*img_x + i] = 0;
			}
		}
	}
}

void invert(std::vector<double> &img, std::vector<double> &out, int img_x, int img_y, unsigned char element[5][5], int element_size) {
	int i, j, k, l;
	for (i = 0; i < img_x; i++) {
		for (j = 0; j < img_y; j++) {
			out[j*img_x + i] = 0;
		}
	}
	for (i = 4; i < img_x - 4; i++) {
		for (j = 4; j < img_y - 4; j++) {
			if (img[j*img_x + i] == 0)
				out[j*img_x + i] = 1;
			else
				out[j*img_x + i] = 0;
		}
	}
}

// As fun��es de manipula��o de arquivo BMP utilizados para salvar as imagens
// foram feitas pelo professor Bogdan, utilizadas em trabalhos acad�micos da UTFPR
/*============================================================================*/
/* DOIS TIPOS PARA MANIPULA��O DE ARQUIVOS BMP                                */
/*----------------------------------------------------------------------------*/
/* Autor: Bogdan T. Nassu - nassu@dainf.ct.utfpr.edu.br                       */
/*============================================================================*/
/** Este arquivo traz declara��es para dois tipos e rotinas para manipula��o de
* arquivos bmp. Como temos um prop�sito puramente did�tico, apenas um sub-
* conjunto m�nimo do formato foi implementado. Matrizes s�o usadas para
* representar os dados. Vetores seriam computacionalmente mais eficientes, mas
* aqui procuramos priorizar a clareza e a facilidade de uso. */
/*============================================================================*/
#define CANAL_R 0 /* Constante usada para se referir ao canal vermelho. */
#define CANAL_G 1 /* Constante usada para se referir ao canal verde. */
#define CANAL_B 2 /* Constante usada para se referir ao canal azul. */

unsigned long getLittleEndianULong(unsigned char* buffer);
int leHeaderBitmap(FILE* stream, unsigned long* offset);
int leHeaderDIB(FILE* stream, unsigned long* largura, unsigned long* altura);
int leDados(FILE* stream, Imagem3C* img);

int salvaHeaderBitmap(FILE* stream, Imagem3C* img);
int salvaHeaderDIB(FILE* stream, Imagem3C* img);
int salvaDados(FILE* stream, Imagem3C* img);
void putLittleEndianULong(unsigned long val, unsigned char* buffer);
void putLittleEndianUShort(unsigned short val, unsigned char* buffer);

/*============================================================================*/
/* Imagem1C                                                                   */
/*============================================================================*/
/** Cria uma imagem vazia.
*
* Par�metros: int largura: largura da imagem.
*             int altura: altura da imagem.
*
* Valor de retorno: a imagem alocada. A responsabilidade por desaloc�-la � do
*                   chamador. */

Imagem1C* criaImagem1C(int largura, int altura)
{
	int i;
	Imagem1C* img;

	img = (Imagem1C*)malloc(sizeof(Imagem1C));

	img->largura = largura;
	img->altura = altura;

	img->dados = (unsigned char**)malloc(sizeof(unsigned char*) * altura);
	for (i = 0; i < altura; i++)
		img->dados[i] = (unsigned char*)malloc(sizeof(unsigned char) * largura);
	return (img);
}

/*----------------------------------------------------------------------------*/
/** Destroi uma imagem dada.
*
* Par�metros: Imagem1C* img: a imagem a destruir.
*
* Valor de retorno: nenhum. */

void destroiImagem1C(Imagem1C* img)
{
	unsigned long i;

	for (i = 0; i < img->altura; i++)
		free(img->dados[i]);
	free(img->dados);
	free(img);
}

/*----------------------------------------------------------------------------*/
/** Abre um arquivo de imagem dado.
*
* Par�metros: char* arquivo: caminho do arquivo a abrir.
*
* Valor de retorno: uma imagem alocada contendo os dados do arquivo, ou NULL
*                   se n�o for poss�vel abrir a imagem. */

Imagem1C* abreImagem1C(char* arquivo)
{
	int i, j;
	Imagem3C* img3c;
	Imagem1C* img1c;

	/* Abre o arquivo como uma imagem de 3 canais. */
	img3c = abreImagem3C(arquivo);
	if (!img3c)
		return (NULL);

	/* Converte a imagem de 3 canais para grayscale. Usamos aqui os mesmos
	fatores de convers�o do OpenCV, que mant�m certas propriedades de
	percep��o. */
	img1c = criaImagem1C(img3c->largura, img3c->altura);
	for (i = 0; i < (long)img3c->altura; i++)
		for (j = 0; j < (long)img3c->largura; j++)
			img1c->dados[i][j] = (unsigned char)(img3c->dados[CANAL_R][i][j] * 0.299 + img3c->dados[CANAL_G][i][j] * 0.587 + img3c->dados[CANAL_B][i][j] * 0.114);

	destroiImagem3C(img3c);
	return (img1c);
}


/*----------------------------------------------------------------------------*/
/** Salva uma imagem em um arquivo dado.
*
* Par�metros: Imagem1C* img: imagem a salvar.
*             char* arquivo: caminho do arquivo a salvar.
*
* Valor de retorno: 0 se ocorreu algum erro, 1 do contr�rio. */

int salvaImagem1C(Imagem1C* img, char* arquivo)
{
	int i, j, ok;

	/* Cria e salva uma imagem de 3 canais. */
	Imagem3C* img3c;
	img3c = criaImagem3C(img->largura, img->altura);

	for (i = 0; i < (long)img->altura; i++)
		for (j = 0; j < (long)img->largura; j++)
		{
			img3c->dados[0][i][j] = img->dados[i][j];
			img3c->dados[1][i][j] = img->dados[i][j];
			img3c->dados[2][i][j] = img->dados[i][j];
		}

	ok = salvaImagem3C(img3c, arquivo);
	destroiImagem3C(img3c);

	return (ok);
}

/*============================================================================*/
/* Imagem3C                                                                   */
/*============================================================================*/
/** Cria uma imagem vazia.
*
* Par�metros: int largura: largura da imagem.
*             int altura: altura da imagem.
*
* Valor de retorno: a imagem alocada. A responsabilidade por desaloc�-la � do
*                   chamador. */

Imagem3C* criaImagem3C(int largura, int altura)
{
	int i, j;
	Imagem3C* img;

	img = (Imagem3C*)malloc(sizeof(Imagem3C));

	img->largura = largura;
	img->altura = altura;

	img->dados = (unsigned char***)malloc(sizeof(unsigned char**) * 3); /* Uma matriz por canal. */
	for (i = 0; i < 3; i++)
	{
		img->dados[i] = (unsigned char**)malloc(sizeof(unsigned char*) * altura);
		for (j = 0; j < altura; j++)
			img->dados[i][j] = (unsigned char*)malloc(sizeof(unsigned char) * largura);
	}

	return (img);
}

/*----------------------------------------------------------------------------*/
/** Destroi uma imagem dada.
*
* Par�metros: Imagem3C* img: a imagem a destruir.
*
* Valor de retorno: nenhum. */

void destroiImagem3C(Imagem3C* img)
{
	unsigned long i, j;

	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < img->altura; j++)
			free(img->dados[i][j]);
		free(img->dados[i]);
	}
	free(img->dados);
	free(img);
}

/*----------------------------------------------------------------------------*/
/** Abre um arquivo de imagem dado.
*
* Par�metros: char* arquivo: caminho do arquivo a abrir.
*
* Valor de retorno: uma imagem alocada contendo os dados do arquivo, ou NULL
*                   se n�o for poss�vel abrir a imagem. */

Imagem3C* abreImagem3C(char* arquivo)
{
	FILE* stream;
	unsigned long data_offset = 0, largura = 0, altura = 0;
	Imagem3C* img;

	/* Abre o arquivo. */
	//stream = fopen(arquivo, "rb");
	
	if (fopen_s(&stream, arquivo, "rb"))
		return (NULL);

	if (!leHeaderBitmap(stream, &data_offset))
	{
		fclose(stream);
		return (NULL);
	}

	if (!leHeaderDIB(stream, &largura, &altura))
	{
		fclose(stream);
		return (NULL);
	}

	/* Pronto, cabe�alhos lidos! Vamos agora colocar o fluxo nos dados... */
	if (fseek(stream, data_offset, SEEK_SET) != 0)
	{
		printf("Error reading file data.\n");
		fclose(stream);
		return (NULL);
	}

	/* ... e tudo pronto para criar nossa imagem! */
	img = criaImagem3C(largura, altura);

	/* L� os dados. */
	if (!leDados(stream, img))
	{
		printf("Error reading data from file.\n");
		fclose(stream);
		free(img);
		return (NULL);
	}

	fclose(stream);
	return (img);
}

/*----------------------------------------------------------------------------*/
/** Pega os 4 primeiros bytes do buffer e coloca em um unsigned long,
* considerando os bytes em ordem little endian.
*
* Par�metros: unsigned char* buffer: l� 4 bytes daqui.
*
* Valor de Retorno: um unsigned long com os dados do buffer reorganizados. */

unsigned long getLittleEndianULong(unsigned char* buffer)
{
	return (buffer[3] << 24) | (buffer[2] << 16) | (buffer[1] << 8) | buffer[0];
}

/*----------------------------------------------------------------------------*/
/** L� o header de 14 bytes do formato BMP.
*
* Par�metros: FILE* stream: arquivo a ser lido. Supomos que j� est� aberto.
*             unsigned long* offset: par�metro de sa�da, � o deslocamento dos
*               dados a partir do in�cio do arquivo.
*
* Valor de Retorno: 1 se n�o ocorreram erros, 0 do contr�rio. */

int leHeaderBitmap(FILE* stream, unsigned long* offset)
{
	unsigned char data[14]; /* O bloco tem exatamente 14 bytes. */

	if (fread((void*)data, 1, 14, stream) != 14)
	{
		printf("Error reading the Bitmap header.\n");
		return (0);
	}

	/* Os 2 primeiros bytes precisam ser 'B' e 'M'. */
	if (data[0] != 'B' || data[1] != 'M')
	{
		printf("Error: can read only BM format.\n");
		return (0);
	}

	/* Vou pular todo o resto e ir direto para o offset. */
	*offset = getLittleEndianULong(&(data[10]));
	return (1);
}

/*----------------------------------------------------------------------------*/
/** L� o header DIB.
*
* Par�metros: FILE* stream: arquivo a ser lido. Supomos que j� est� aberto.
*             unsigned long* largura: par�metro de sa�da. Largura da imagem.
*             unsigned long* altura: par�metro de sa�da. Altura da imagem.
*
* Valor de Retorno: 1 se n�o ocorreram erros, 0 do contr�rio. */

int leHeaderDIB(FILE* stream, unsigned long* largura, unsigned long* altura)
{
	unsigned long size = 0; /* O tamanho do cabe�alho DIB. */

	if (fread((void*)&size, 4, 1, stream) != 1)
	{
		printf("Error reading DIB header.\n");
		return (0);
	}

	if (size == 12) /* Formato BITMAPCOREHEADER. */
	{
		printf("Error: BITMAPCOREHEADER not supported (is this file really THAT old!?)\n");
		return (0);
	}
	else if (size >= 40) /* Outros formatos. */
	{
		unsigned short tmp_short = 0;
		unsigned long tmp_long = 0;

		/* Largura. */
		if (fread((void*)largura, 4, 1, stream) != 1 || *largura <= 0)
		{
			printf("Error: invalid width.\n");
			return (0);
		}

		/* Altura. */
		if (fread((void*)altura, 4, 1, stream) != 1 || *altura <= 0)
		{
			printf("Error: invalid height.\n");
			return (0);
		}

		/* Color planes. Precisa ser 1. */
		if (fread((void*)&tmp_short, 2, 1, stream) != 1 || tmp_short != 1)
		{
			printf("Error reading DIB header.\n");
			return (0);
		}

		/* Bpp. Aqui, estou for�ando 24 bpp. */
		if (fread((void*)&tmp_short, 2, 1, stream) != 1 || tmp_short != 24)
		{
			printf("Error: this function supports only 24 bpp files.\n");
			return (0);
		}

		/* Compress�o. Vou aceitar s� imagens sem compress�o. */
		if (fread((void*)&tmp_long, 4, 1, stream) != 1 || tmp_long != 0)
		{
			printf("Error: this function supports only uncompressed files.\n");
			return (0);
		}

		/* Pula os pr�ximos 12 bytes. */
		if (fseek(stream, 12, SEEK_CUR) != 0)
		{
			printf("Error reading DIB header.\n");
			return (0);
		}

		/* Paleta. N�o � para usar! */
		if (fread((void*)&tmp_long, 4, 1, stream) != 1 || tmp_long != 0)
		{
			printf("Error: this function does not support color palettes.\n");
			return (0);
		}

		return (1);
	}

	return (0);
}

/*----------------------------------------------------------------------------*/
/** L� os dados de um arquivo.
*
* Par�metros: FILE* stream: arquivo a ser lido. Supomos que j� est� aberto.
*             Imagem3C* img: imagem a preencher.
*
* Valor de Retorno: 1 se n�o ocorreram erros, 0 do contr�rio. */

int leDados(FILE* stream, Imagem3C* img)
{
	long long i, j;
	int line_padding;

	/* Calcula quantos bytes preciso pular no fim de cada linha.
	Aqui, cada linha precisa ter um m�ltiplo de 4. */
	line_padding = (int)ceil(img->largura*3.0 / 4.0) * 4 - (img->largura * 3);

	/* L�! */
	for (i = img->altura - 1; i >= 0; i--)
	{
		for (j = 0; j < img->largura; j++)
		{
			if (fread(&(img->dados[CANAL_B][i][j]), 1, 1, stream) != 1)
				return (0);

			if (fread(&(img->dados[CANAL_G][i][j]), 1, 1, stream) != 1)
				return (0);

			if (fread(&(img->dados[CANAL_R][i][j]), 1, 1, stream) != 1)
				return (0);
		}

		if (fseek(stream, line_padding, SEEK_CUR) != 0)
			return (0);
	}

	return (1);
}

/*----------------------------------------------------------------------------*/
/** Salva uma imagem em um arquivo dado.
*
* Par�metros: Imagem3C* img: imagem a salvar.
*             char* arquivo: caminho do arquivo a salvar.
*
* Valor de retorno: 0 se ocorreu algum erro, 1 do contr�rio. */

int salvaImagem3C(Imagem3C* img, char* arquivo)
{
	FILE* stream;

	/* Abre o arquivo. */
	//stream = fopen(arquivo, "wb");
	if (fopen_s(&stream, arquivo, "wb"))
		return (0);

	/* Escreve os blocos. */
	if (!salvaHeaderBitmap(stream, img))
	{
		fclose(stream);
		return (0);
	}

	if (!salvaHeaderDIB(stream, img))
	{
		fclose(stream);
		return (0);
	}

	if (!salvaDados(stream, img))
	{
		fclose(stream);
		return (0);
	}

	fclose(stream);
	return (1);
}

/*----------------------------------------------------------------------------*/
/** Coloca um unsigned long nos 4 primeiros bytes do buffer, em ordem little
* endian.
*
* Par�metros: unsigned long val: valor a escrever.
*             unsigned char* buffer: coloca o valor aqui.
*
* Valor de Retorno: NENHUM */

void putLittleEndianULong(unsigned long val, unsigned char* buffer)
{
	buffer[0] = (unsigned char)val;
	buffer[1] = (unsigned char)(val >> 8);
	buffer[2] = (unsigned char)(val >> 16);
	buffer[3] = (unsigned char)(val >> 24);
}

/*----------------------------------------------------------------------------*/
/** Coloca um unsigned short nos 2 primeiros bytes do buffer, em ordem little
* endian.
*
* Par�metros: unsigned short val: valor a escrever.
*             unsigned char* buffer: coloca o valor aqui.
*
* Valor de Retorno: NENHUM */

void putLittleEndianUShort(unsigned short val, unsigned char* buffer)
{
	buffer[0] = (unsigned char)val;
	buffer[1] = (unsigned char)(val >> 8);
}

/*----------------------------------------------------------------------------*/
/** Escreve o header Bitmap.
*
* Par�metros: FILE* file: arquivo a ser escrito. Supomos que j� est� aberto.
*             Imagem3C* img: imagem a ser salva.
*
* Valor de Retorno: 1 se n�o ocorreram erros, 0 do contr�rio. */

int salvaHeaderBitmap(FILE* stream, Imagem3C* img)
{
	unsigned char data[14]; /* O bloco tem exatamente 14 bytes. */
	int pos = 0;
	unsigned long bytes_por_linha;

	data[pos++] = 'B';
	data[pos++] = 'M';

	/* Tamanho do arquivo. Definimos como sendo 14+40 (dos cabe�alhos) + o espa�o dos dados. */
	bytes_por_linha = (unsigned long)ceil(img->largura*3.0 / 4.0) * 4;
	putLittleEndianULong(14 + 40 + img->altura*bytes_por_linha, &(data[pos]));
	pos += 4;

	/* Reservado. */
	putLittleEndianULong(0, &(data[pos]));
	pos += 4;

	/* Offset. Definimos como 14+40 (o tamanho dos cabe�alhos). */
	putLittleEndianULong(14 + 40, &(data[pos]));

	if (fwrite((void*)data, 1, 14, stream) != 14)
	{
		printf("Error writing Bitmap header.\n");
		return (0);
	}

	return (1);
}

/*----------------------------------------------------------------------------*/
/** Escreve o header DIB.
*
* Par�metros: FILE* file: arquivo a ser escrito. Supomos que j� est� aberto.
*             Imagem3C* img: imagem a ser salva.
*
* Valor de Retorno: 1 se n�o ocorreram erros, 0 do contr�rio. */

int salvaHeaderDIB(FILE* stream, Imagem3C* img)
{
	unsigned char data[40]; /* O bloco tem exatamente 40 bytes. */
	int pos = 0;
	unsigned long bytes_por_linha;

	/* Tamanho do header. Vamos usar um BITMAPINFOHEADER. */
	putLittleEndianULong(40, &(data[pos]));
	pos += 4;

	/* Largura. */
	putLittleEndianULong(img->largura, &(data[pos]));
	pos += 4;

	/* Altura. */
	putLittleEndianULong(img->altura, &(data[pos]));
	pos += 4;

	/* Color planes. */
	putLittleEndianUShort(1, &(data[pos]));
	pos += 2;

	/* bpp. */
	putLittleEndianUShort(24, &(data[pos]));
	pos += 2;

	/* Compress�o. */
	putLittleEndianULong(0, &(data[pos]));
	pos += 4;

	/* Tamanho dos dados. */
	bytes_por_linha = (unsigned long)ceil(img->largura*3.0 / 4.0) * 4;
	putLittleEndianULong(img->altura*bytes_por_linha, &(data[pos]));
	pos += 4;

	/* Resolu��o horizontal e vertical (simplesmente copiei este valor de algum arquivo!). */
	putLittleEndianULong(0xF61, &(data[pos]));
	pos += 4;
	putLittleEndianULong(0xF61, &(data[pos]));
	pos += 4;

	/* Cores. */
	putLittleEndianULong(0, &(data[pos]));
	pos += 4;
	putLittleEndianULong(0, &(data[pos]));
	pos += 4;

	if (fwrite((void*)data, 1, 40, stream) != 40)
	{
		printf("Error writing DIB header.\n");
		return (0);
	}

	return (1);
}

/*----------------------------------------------------------------------------*/
/** Escreve o bloco de dados.
*
* Par�metros: FILE* file: arquivo a ser escrito. Supomos que j� est� aberto.
*             Imagem3C* img: imagem a ser salva.
*
* Valor de Retorno: 1 se n�o ocorreram erros, 0 do contr�rio. */

int salvaDados(FILE* stream, Imagem3C* img)
{
	long long i, j;
	unsigned long largura_linha, line_padding;
	unsigned char* linha;
	unsigned long pos_linha;

	/* Calcula quantos bytes preciso pular no fim de cada linha.
	Aqui, cada linha precisa ter um m�ltiplo de 4. */
	largura_linha = (unsigned long)ceil(img->largura*3.0 / 4.0) * 4;
	line_padding = largura_linha - (img->largura * 3);
	linha = (unsigned char*)malloc(sizeof(unsigned char) * largura_linha);

	for (i = img->altura - 1; i >= 0; i--)
	{
		pos_linha = 0;
		for (j = 0; j < img->largura; j++)
		{
			linha[pos_linha++] = img->dados[CANAL_B][i][j];
			linha[pos_linha++] = img->dados[CANAL_G][i][j];
			linha[pos_linha++] = img->dados[CANAL_R][i][j];
		}

		for (j = 0; j < line_padding; j++)
			linha[pos_linha++] = 0;

		if (fwrite((void*)linha, 1, largura_linha, stream) != largura_linha)
		{
			printf("Error writing image data.\n");
			free(linha);
			return (0);
		}
	}

	free(linha);
	return (1);
}



/*============================================================================*/

// Fun��es de leitura de arquivos tif dispobinilizadas no site que hospeda
// as databases utilizadas no artigo. Arquivo dispon�vel em:
// http://bias.csr.unibo.it/fvc2004/Downloads/sourceFVC.zip
/* ----------------------- */
/*    Auxiliary routines
/* ----------------------- */

unsigned char buffer[512];

unsigned int in_dword(unsigned int i)
{
	unsigned int v = 0;

	v = v | (buffer[i]);
	v = v | (buffer[i + 1] << 8);
	v = v | (buffer[i + 2] << 16);
	v = v | (buffer[i + 3] << 24);
	return v;
}

unsigned int in_word(unsigned int i)
{
	unsigned int v = 0;

	v = v | (buffer[i]);
	v = v | (buffer[i + 1] << 8);
	return v;
}


// Load a 256 gray-scale uncompressed TIF image into the global array IMAGE
int Load_gray256tif(FILE* fp, bool Upright, int img_x, int img_y){
	unsigned int ifd_offset;
	unsigned int directory_entry_count;
	unsigned int offset;
	unsigned int strip_offset, data_offset;
	bool strip_based = false;
	unsigned char* pimg;
	int i;

	if (fread(buffer, 8, 1, fp) != 1) return 1;
	if (in_word(0) != 0x4949) return 2;
	if (in_word(2) != 0x002a) return 3;
	ifd_offset = in_dword(4);
	if (fseek(fp, ifd_offset, SEEK_SET)) return 1;
	if (fread(buffer, 2, 1, fp) != 1) return 1;
	directory_entry_count = in_word(0);
	if (fread(buffer, directory_entry_count * 12, 1, fp) != 1) return 1;
	offset = 0;
	while (directory_entry_count >0)
	{
		switch (in_word(offset))
		{
		case 0x00fe: if (in_word(offset + 8) != 0) return 4; break;
		case 0x0100: img_x = in_word(offset + 8); break;
		case 0x0101: img_y = in_word(offset + 8); break;
		case 0x0102: if (in_word(offset + 8) != 8) return 5; break;
		case 0x0103: if (in_word(offset + 8) != 1) return 6; break;
		case 0x0106: if (in_word(offset + 8) != 1) return 7; break;
		case 0x0111: strip_offset = in_word(offset + 8); break;
		case 0x0115: if (in_word(offset + 8) != 1) return 8; break;
		case 0x0116: if (in_word(offset + 8) != img_y) strip_based = true; break;
		case 0x011c: if (in_word(offset + 8) != 1) return 11; break;
		}
		offset += 12;
		directory_entry_count -= 1;
	}

	if (strip_based)
	{
		if (fseek(fp, strip_offset, SEEK_SET)) return 1;
		if (fread(buffer, 4, 1, fp) != 1) return 1;
		data_offset = in_dword(0);
	}
	else data_offset = strip_offset;
	if (fseek(fp, data_offset, SEEK_SET)) return 1;

	if (Upright)
	{
		pimg = IMAGE;
		for (i = 0; i<img_y; i++)
		{
			if (fread(pimg, img_x, 1, fp) != 1) return 1;
			pimg += img_x;
		}
	}
	else
	{
		pimg = IMAGE + img_x*(img_y - 1);
		for (i = 0; i<img_y; i++)
		{
			if (fread(pimg, img_x, 1, fp) != 1) return 1;
			pimg -= img_x;
		}
	}
	return 0;
}